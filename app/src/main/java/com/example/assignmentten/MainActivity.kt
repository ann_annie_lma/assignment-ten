package com.example.assignmentten

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.assignmentten.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViewPagerAdapter()

    }

    private fun setUpViewPagerAdapter()
    {
        val adapter = ViewPagerAdapter()
        adapter.setData(loadData())
        binding.vpGallery.adapter = adapter
        binding.vpGallery.orientation = ViewPager2.ORIENTATION_VERTICAL

        binding.vpGallery.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                //it's wrong but i could not use resource string with placeholder
                //???
                binding.tvPosition.setText("${position + 1}/${loadData().size}")
            }
        })

    }

    private fun loadData():List<Movie>
    {
        return listOf(
            Movie(R.mipmap.columbus,
                resources.getString(R.string.columbus),
                resources.getString(R.string.columbus_description)),

            Movie(R.mipmap.dogville,
                resources.getString(R.string.dogville),
                resources.getString(R.string.dogville_description)),

            Movie(R.mipmap.ending_things,
                resources.getString(R.string.ending_things),
                resources.getString(R.string.ending_things_description)),

            Movie(R.mipmap.imitation_game,
                resources.getString(R.string.imitation_game),
                resources.getString(R.string.imitation_game_description)),


            Movie(R.mipmap.night_on_earth,
                resources.getString(R.string.night_on_earth),
                resources.getString(R.string.night_on_earth_description)),

            Movie(R.mipmap.paterson,
                resources.getString(R.string.paterson),
                resources.getString(R.string.paterson_description))
        )
    }


}