package com.example.assignmentten

import android.annotation.SuppressLint
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignmentten.databinding.ViewpagerItemBinding

class ViewPagerAdapter : RecyclerView.Adapter<ViewPagerAdapter.ViewPagerViewHolder>() {

    private val movieList = mutableListOf<Movie>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerViewHolder {
        return ViewPagerViewHolder(
            ViewpagerItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun onBindViewHolder(holder: ViewPagerViewHolder, position: Int) {
        holder.bindData()
    }

    override fun getItemCount(): Int = movieList.size

    inner class ViewPagerViewHolder(val binding: ViewpagerItemBinding):
        RecyclerView.ViewHolder(binding.root)
    {
            fun bindData()
            {
                val currentMovie = movieList[adapterPosition]
                binding.ivMovieImage.setImageResource(currentMovie.imageResource)
                binding.tvMovieTitle.text = currentMovie.movieTitle.toString()
                binding.tvDescription.text = currentMovie.movieDescription.toString()

                //it's wrong but i could not use resource string with placeholder
                //???
            }


        }


    fun setData(movies:List<Movie>)
    {
        this.movieList.clear()
        this.movieList.addAll(movies)
        notifyDataSetChanged()
    }
}